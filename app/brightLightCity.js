HoneyPot.BrightLightCityGame = function(){
    HoneyPot.Game.call(this);
    this.getProgressives = false;
    this.state = 'base';
    this.bonusState = false; //might not need this
}

HoneyPot.BrightLightCityGame.prototype = Object.create(HoneyPot.Game.prototype);
HoneyPot.BrightLightCityGame.prototype.constructor = HoneyPot.BrightLightCityGame;

HoneyPot.BrightLightCityGame.prototype.welcomeTransitionComplete = function(){
    HoneyPot.Game.prototype.welcomeTransitionComplete.call(this);
    this.getProgressives = !this.connection.freePlay; 
    this.app.playSound('bgSound',true,0.8);
}

HoneyPot.BrightLightCityGame.prototype.startGIP = function(){
    this.canvas.ticker.maxFPS = this.gameMaxFPS;
    this.bonusWinCount = 0;
    if(this.gameState.slotState && this.gameState.slotState.action === 'freeSpinReels'){
        this.topBar.setwin(this.gameState.slotState.totalWinnings - this.gameState.slotState.currentWinnings);
        this.gameUI.setWinText(this.gameState.slotState.totalWinnings - this.gameState.slotState.currentWinnings);
        this.screens['reelScreen'].currentWinnings = this.gameState.slotState.totalWinnings - this.gameState.slotState.currentWinnings;
        this.gameUI.showFreeSpins(this.gameState.slotState.numFreeSpins);
        this.gameState.inFreeSpins = true;
        //
    }else{
        this.gameUI.fadeOutButtons();
    }
    this.topBar.gameStarted(true);
    this.screens['reelScreen'].spin();
    this.playRequestReceived();
}

HoneyPot.BrightLightCityGame.prototype.closeBonus = function(){
    this.screens['reelScreen'].endFreeSpins();
    this.screens['reelScreen'].closeSpin();
}

HoneyPot.BrightLightCityGame.prototype.sendCloseRequest = function(){
	if(this.gameState.inFreeSpins){ 
		if(this.gameUI.inAutoPlay()){
			this.gameUI.stopAutoplay();
		}
		if(this.gameState.slotState.numFreeSpins <=0){
			this.gameState.inFreeSpins = false;
			this.screens[this.currentScreen].endFreeSpins(this.closeFreeSpins.bind(this)); 
			this.gameUI.buttonPanelUI.alpha = 0;
			this.gameUI.disableButtons(); 
			this.gameUI.hideFreeSpins();
		}else{
			this.topBar.gameStarted(false);
			this.topBar.setwin(this.gameState.totalWin);
			this.gameUI.showFreeSpins(this.gameState.slotState.numFreeSpins-1);
			this.startPlay(); 
		}
	}else{  
		if(this.gameState.totalWin>0  && !this.connection.serverError){
			this.gameUI.showTotalWin(this.gameState.totalWin);
		}else{
			this.canvas.ticker.maxFPS = this.gameMinFPS;
			this.connection.sendCloseRequest(this.closeResponse.bind(this));
		}
	}
}

HoneyPot.BrightLightCityGame.prototype.sendInitChainGameRequest = function() {
	this.connection.sendInitChainGame();
}
