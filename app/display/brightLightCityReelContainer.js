HoneyPot.BrightLightCityReelContainer = function(data){
	HoneyPot.ReelContainer.call(this,data);
	this.anticipationSound;
}

HoneyPot.BrightLightCityReelContainer.prototype = Object.create(HoneyPot.ReelContainer.prototype);
HoneyPot.BrightLightCityReelContainer.prototype.constructor = HoneyPot.BrightLightCityReelContainer;

HoneyPot.BrightLightCityReelContainer.prototype.init = function(reelId){
	HoneyPot.ReelContainer.prototype.init.call(this, reelId);
	this.anticipator = this.getComponentByID('anticipator');
	this.anticipationSound = PIXI.sound.Sound.from('assets/sounds/anticipation.m4a');

	this.mirrorSym = []
}

HoneyPot.BrightLightCityReelContainer.prototype.stopSpin = function(stopPos, callback){
	this.allreelsStoppedCallback = callback;
	this.symBonusRedCountArr = ['x','x','x','x','x'];

	var anticipation = 0; 

	var bonusRedSymCount = 0;

	this.bonusRedSymCount = 0;

	this.bonusRedLandCount = 0;
	
	this.stopPos = stopPos;

	this.symBonusRedCountArr = this.checkAnticipationSymbol(this.stopPos,10);

	if(this.state === 'spin'){
		this.reelStopCount = 0;
		for(var i=0;i<this.reels.length;i++){
			this.reels[i].setReelSet(this.reelSet[i]); 
			this.reels[i].stopPosition = stopPos[i];
			
			if(bonusRedSymCount === 2 && i === 4){
				anticipation += 3000;
			}	

			this.reels[i].stopAnticipation = anticipation; 
			
			this.reels[i].stopReel(this.reelStopped.bind(this));

			if(this.symBonusRedCountArr[i] !== 'x'){
				this.bonusRedLandCount += 1;
			}
			if(this.symBonusRedCountArr[i] !== 'x'){
				bonusRedSymCount +=1;
			}
		}
		this.state = 'stop';
	}
}

HoneyPot.BrightLightCityReelContainer.prototype.checkAnticipationSymbol = function(stop, symbol){
	var reelList = [];
	var rs;
	var pos;
	if(game.gameState.inFreeSpins !== true){
		for(var i=0; i <this.reels.length; i++){
			rs = this.reelSet[i];  
			pos = Number(stop[i]); 
			reelList[i] = 'x';
			for(var r=0;r<3;r++){
				pos = r+Number(stop[i]);
				if(pos >= rs.length){
					pos = pos - rs.length;
				}
				if(parseInt(rs[pos]) === symbol){
					reelList[i] = r;
				}
			}
		}
	}else{
		for(var reelIdx=0; reelIdx <this.reels.length; reelIdx++){
			rs = this.reelSet[reelIdx];
			pos = Number(stop[reelIdx]);
			reelList[reelIdx] = 'x';

			if(reelIdx !== 1 && reelIdx !== 3){
				for(var r=0;r<3;r++){
					pos = r+Number(stop[reelIdx]);
					if(pos >= rs.length){
						pos = pos - rs.length;
					}
					if(rs[pos] == symbol){
					reelList[reelIdx] = r;
					}
				}
			}else{
				for(var r=0;r<3;r++){
					if(r !== 1){
						pos = r+Number(stop[reelIdx]);
						if(pos >= rs.length){
							pos = pos - rs.length;
						}
						if(rs[pos] === symbol){
						reelList[reelIdx] = r;
						}
					}
				}
			}
		}
	}
	return reelList;
}

HoneyPot.BrightLightCityReelContainer.prototype.reelStopped = function(reelId){
	if(this.symBonusRedCountArr[this.reelStopCount] !== 'x') {
		this.bonusRedSymCount +=1 ;
	}

	this.reelStopCount += 1; 

	if(this.bonusRedSymCount >=2 && this.reelStopCount !==5){
		if(this.reelStopCount === 4){
				if((this.reelStopCount)<this.reels.length ){
					this.reels[(this.reelStopCount)].anticipate = true;
				}
				//this.anticipationMovie.lastTime = null;
				//this.anticipationMovie.state.setAnimation(0,'animation',true);
				this.anticipator.x = 470;
				this.anticipator.visible = true; 
				//TweenLite.to(this.anticipationMovie, 1,{ease: Expo.easeOut ,x:802});
				if(this.anticipationSound.isPlaying == false){
					if(game.gameState.inFreeSpins == true){
						this.app.stopSound('freespinBG');
					}else{
						this.app.stopSound('bgSound');
					}
					this.anticipationSound.play();
				}
		}else{
			if(this.anticipationSound.isPlaying == false){
				if(game.gameState.inFreeSpins == true){
					this.app.stopSound('freespinBG');
				}else{
					this.app.stopSound('bgSound');
				}
				this.anticipationSound.play();
			}
			//TweenLite.to(this.anticipationMovie, 1,{pixi:{positionX:950}, ease: Expo.easeOut});
		}	
	}

	if(this.reelStopCount == this.reels.length) {

		if (this.anticipator.visible == true){
			this.anticipator.visible = false;
			this.app.playSound('anticipationSoundEnd');
			if (game.gameState.inFreeSpins == true){
				this.app.playSound('freespinBG', true, 0.8);
			} else {
				this.app.playSound('bgSound', true, 0.8);
			}
		}

		if(game.gameState.inFreeSpins === true && game.gameState.slotState.slotOverlays.length > 0){
           this.animateMirrors(this.reels);
        }else{
            this.state = 'idle';
    	    this.allreelsStoppedCallback();
       }
	}
}

HoneyPot.BrightLightCityReelContainer.prototype.showMirrors = function(){
	for(var i = 0; i < this.mirrorSym.length; i++){
		for(var j = 0; j < this.chosenMirrorNum.length; j ++){
			if(i == this.chosenMirrorNum[j]){
				this.mirrorSym[i].mirror.visible = true;
			    this.mirrorSym[i].bonusSym.visible = true;
			}
		}
	}
}

HoneyPot.BrightLightCityReelContainer.prototype.animateReelSymbols = function(winline, matchPos){
    this.chosenMirrorNum = [];
    for(var col=0;col<this.reels.length;col++){
	    this.reels[col].setHideAlpha();
		if(matchPos[col] != 'x'){
			if(0 <= this.mirrorSym.length){
                for(var ol=0; ol < this.mirrorSym.length; ol++){
	                if(col == this.mirrorSym[ol].col && matchPos[col] == this.mirrorSym[ol].row){
	                	this.chosenMirrorNum.push(ol)
	                	this.mirrorSym[ol].bonusSym.visible = false
	                	this.mirrorSym[ol].mirror.visible = false
	                }
	            }
            }
		    this.reels[col].animateSymbol(parseInt(winline[col])+1);
		 }
	}
}

HoneyPot.BrightLightCityReelContainer.prototype.animateMirrors = function(reels){
	this.reels = reels
    this.reelCount = 0;
    this.mirrorSym = []
    for(var col=0; col< game.gameState.slotDef.numReels; col++){
        for(var row = 0; row < game.gameState.slotDef.viewSize; row++){
            var symbolNum = game.gameState.slotState.slotOverlays[0].symbolIndex;
            var symbol = new HoneyPot.AnimatedSprite({image:this.app.getAssetById('symScarabPileAnim'), anchor:0.5,animationSpeed:0.5});
            var bonusSym = new HoneyPot.AnimatedSprite({image:this.app.getAssetById(this.reels[0].slotTexturesNames[symbolNum]), anchor:0.5,animationSpeed:0.5});
            symbol.x = 65 + (col * 140);
            symbol.y = 50+ (row * 101);
            bonusSym.x = 65+ (col * 140);
            bonusSym.y = 50+ (row * 101);

            for(var ol=0; ol<game.gameState.slotState.slotOverlays.length; ol++){
            	if(col == game.gameState.slotState.slotOverlays[ol].column && row == game.gameState.slotState.slotOverlays[ol].row){
            		this.addChild(symbol);
                    this.addChild(bonusSym);
                    this.mirrorSym.push({mirror:symbol, bonusSym: bonusSym, col:col, row:row});
                    symbol.gotoAndPlay(0);
                        if(ol == game.gameState.slotState.slotOverlays.length-1){
                        	 TweenLite.to(bonusSym,1,{delay:0.3,pixi:{alpha:0.8, scale:0.7},onComplete:this.animateMirrorsComplete.bind(this)})
                        }
                    }else{
                    	    TweenLite.to(bonusSym,1,{delay:0.3,pixi:{alpha:0.8, scale:0.7}})

            	}
            }

        }
    }
}

HoneyPot.BrightLightCityReelContainer.prototype.animateMirrorsComplete = function(){
    	this.state = 'idle';
	    this.allreelsStoppedCallback();
}

HoneyPot.BrightLightCityReelContainer.prototype.resetMirrors = function(){
	for(var msIdx = 0; msIdx <this.mirrorSym.length; msIdx++){
        this.mirrorSym[msIdx].mirror.visible = false;
        this.mirrorSym[msIdx].bonusSym.visible = false;
    }
    this.mirrorSym = [];
}