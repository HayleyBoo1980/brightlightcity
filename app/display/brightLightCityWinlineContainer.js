HoneyPot.BrightLightCityAnimatedWinlineContainer = function(data){
    HoneyPot.AnimatedWinlineContainer.call(this,data);
}

HoneyPot.BrightLightCityAnimatedWinlineContainer.prototype = Object.create(HoneyPot.AnimatedWinlineContainer.prototype);
HoneyPot.BrightLightCityAnimatedWinlineContainer.prototype.constructor = HoneyPot.BrightLightCityAnimatedWinlineContainer;

HoneyPot.BrightLightCityAnimatedWinlineContainer.prototype.animateWinlineArrowComplete = function(){
	this.removeChild(this.winlineArrow);
	game.screens['reelScreen'].reelContainer.showMirrors();
}