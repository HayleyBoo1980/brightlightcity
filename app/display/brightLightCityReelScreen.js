HoneyPot.BrightLightCityReelScreen = function(data){
    HoneyPot.ReelScreen.call(this,data);
    this.gameState = undefined;
    this.bluePotText = undefined;
}

HoneyPot.BrightLightCityReelScreen.prototype = Object.create(HoneyPot.ReelScreen.prototype);
HoneyPot.BrightLightCityReelScreen.prototype.constructor = HoneyPot.BrightLightCityReelScreen;

HoneyPot.BrightLightCityReelScreen.prototype.initComponents = function(){
    HoneyPot.ReelScreen.prototype.initComponents.call(this);
    
    this.reelContainer = this.getComponentByID('reelContainer');
    this.bonusMapStart = this.getComponentByID('bonusMapStart');
    this.bonusMapStart.scale = 0;
    this.bonusMapStart.on('pointerdown', this.bonusChosen.bind(this))
    this.hitOrMissBonus = this.getComponentByID('brightLightCityHitOrMiss');
}

/*KEEPING IN BECAUSE OF DIFFERENT WINLINE SOUNDS*/
HoneyPot.BrightLightCityReelScreen.prototype.updateShowWinline = function(delta){
    if(this.state === 'showWinlines'){
        this.slotDef = this.gameState.getSlotDef();
        this.winlineTimer -= 1;
        if(this.winlineTimer === 0){
            if(this.winlineContainer){
                this.winlineContainer.removeAllWinlines();
            }
            this.reelContainer.resetReelSymbols();
            this.hideWinPanel();
            if(this.winlineCounter >= this.wins.length){
                this.reelContainer.interactive = false;
                this.reelContainer.buttonMode = false;
                this.closeSpin();
            }else{
                this.updateWinText(this.wins[this.winlineCounter].winnings);
                this.showWinPanel(this.winlineCounter);
                if(this.winlineContainer){
                    this.winlineContainer.showWinline(this.wins[this.winlineCounter].winLine,this.wins[this.winlineCounter]);
                }            
                this.reelContainer.animateReelSymbols(this.slotDef.winLines[this.wins[this.winlineCounter].winLine].winlinePattern,this.wins[this.winlineCounter].matchPositions);
                
                if(this.winlineCounter < 10){
                    this.app.playSound('winLine'+this.winlineCounter);
                }else{
                    this.app.playSound('winLine'+this.winlineCounter - 10);
                }
            }
            this.winlineCounter += 1;
            this.winlineTimer = this.winlineDelay;
        }
    }
}

HoneyPot.BrightLightCityReelScreen.prototype.showScatterBonus = function(){
    if(this.scatterWin.slotBonusWin.length!=0){ 
        if(game.gameUI.inAutoPlay()){
            game.gameUI.stopAutoplay();
            game.gameUI.buttonPanelUI.visible = false;
        }
        if(game.gameState.inFreeSpins){
            game.gameUI.buttonPanelUI.visible = false;
        }

        this.bonusDef = this.gameState.getSlotDef().getSlotBonusByID(this.scatterWin.slotBonusWin[0].bonusId);

        switch(this.bonusDef.name){ 
            case 'TigerWildReelBonus':
            case 'BeerGogglesFreeSpins':
                this.bonusMapStart.visible = true;
                TweenLite.to(this.bonusMapStart,1,{pixi:{scale:1}});
                break;              
        }
    }else{
        this.closeSpin();
    }
}

HoneyPot.BrightLightCityReelScreen.prototype.bonusChosen = function(){
       switch(this.gameState.chainPlay.gameName){
            case 'TigerWildReelBonus':
            case 'BeerGogglesFreeSpins':
            console.log('blahs blahs ')
                break;
            case'BrightLightCityHitItOrQuitItBonus':
                console.log(this.gameState.slotState.slotScatterWins[0].slotBonusWin[0])
                this.bonusMapStart.visible = false;
                this.hitOrMissBonus.init();
               // game.sendInitChainGameRequest();
                //this.chainConnection = new HoneyPot.OpenBetReelChainConnection();
               // this.reelContainer.getChainHeaderDetails();

               // this.closeSpin();
       }
}

HoneyPot.BrightLightCityReelScreen.prototype.startFreeSpins = function(){
    this.bonusMapStart.visible = false;
    TweenLite.to(this.bonusMapStart,1,{pixi:{scale:0}});
    this.showFreeSpins(false);
    this.gameState.inFreeSpins = true;
    this.closeSpin();
}

HoneyPot.BrightLightCityReelScreen.prototype.showFreeSpins = function(isGIP){
    if(game.gameUI.buttonPanelUI.visible == false){
        game.gameUI.buttonPanelUI.visible = true
    }
}

HoneyPot.BrightLightCityReelScreen.prototype.closeSpin = function(){
	if(this.reelContainer.mirrorSym.length != 0){
    	 this.reelContainer.resetMirrors();
    }
    HoneyPot.ReelScreen.prototype.closeSpin.call(this);
}

HoneyPot.BrightLightCityReelScreen.prototype.endFreeSpins = function(){
    game.gameUI.hideFreeSpins();
    game.gameUI.buttonPanelUI.alpha = 1;
    game.gameUI.enableButtons();
    this.closeSpin();
}