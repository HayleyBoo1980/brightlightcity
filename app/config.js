var gameConfig = {
    game:{
        version:"1.0.0",
        gameName:"BrightLightCity",
        gameType:"slot",
        channel:"I",
        gameWidth:1024,
        gameHeight:576,
        antialias:true,
        logLevel:1,
        transparent:false,
        forceCanvas:true,
        resolution:1,
        minFPS:24,
        maxFPS:60,
        topBar:{
          type:'GCMTopBar',
        }
    },
    connection:{
        connectionURL: "https://fog-integration-casino-games-8.casino.openbet.com//cc_head/fog/Servlet",
        freeBets: "Yes",
        freePlay: true,
        isGuest: "No",
        cookie:"aTXR2EPh0csQC3Sqop8zUNueAaeGyWVY2tVBUkmopnCKVcN6GAdzEXVG3xx5jdG38wWvzxs=", //hungryBear301
        connectionType: "BLCOpenBetChainConnection",
        gameClass: "Slot",
        gameName: "BrightLightCity",
        freeGameName: "FreeBrightLightCity",
        channel: "I",
        connectionTimeOut: 30000
    },
    assets:{
        images:{
            files:[
                //WelcomeScreen
                {name:"preLoaderLogo", url:"assets/images/preloaderLogo.png"},
                //Symbol
                {name:"symLogo", url:"assets/images/symbols/symLogo.png"},
                {name:"symTreasure", url:"assets/images/symbols/symTreasure.png"},
                {name:"symBeerGoggles", url:"assets/images/symbols/symBeerGoggles.png"},
                {name:"symElvis", url:"assets/images/symbols/symAnkh.png"},
                {name:"symBonusRed", url:"assets/images/symbols/symBonusRed.png"},
                {name:"symNightWorker", url:"assets/images/symbols/symNightWorker.png"},
                {name:"symTattoo", url:"assets/images/symbols/symTattoo.png"},
                {name:"symAce", url:"assets/images/symbols/symAce.png"},
                {name:"symKing", url:"assets/images/symbols/symKing.png"},
                {name:"symQueen", url:"assets/images/symbols/symQueen.png"},
                {name:"symTen", url:"assets/images/symbols/symTen.png"},
                {name:"symJack", url:"assets/images/symbols/symJack.png"},
                {name:"symWild", url:"assets/images/symbols/symWild.png"},
                {name:"symBonusBlue",url:"assets/images/symbols/symBonusBlue.png"},
                //SymAnimations
                {name:"symScarabAnim", url:"assets/images/symbols/symScarabAnim.png"},
                {name:"symMummyLaurenAnim", url:"assets/images/symbols/symMummyLaurenAnim.png"},
                {name:"symScarabPileAnim",url:"assets/images/symbols/symScarabPileAnim.png"},
                {name:"symAnkhAnim", url:"assets/images/symbols/symAnkhAnim.png"},
                {name:"symTreasureAnim", url:"assets/images/symbols/symTreasureAnim.png"},
                {name:"symWildAnim", url:"assets/images/symbols/symWildAnim.png"},
                {name:"symLogoAnim", url:"assets/images/symbols/symLogoAnim.png"},
                //BaseGame
                {name:"welcomeBG", url:"assets/images/welcomeBG.png"},
                {name:"gameBG", url:"assets/images/gameBG.png"},
                {name:"reelsFrame", url:"assets/images/reelsFrame.png"},
                {name:"anticipator", url:"assets/images/anticipation.png"},
                {name:"gameBorder", url:"assets/images/gameBorder.png"},
                //BonusMap
                {name:"bonusMapStart", url:"assets/images/bonusMapStart.png"},
                //HitOrMiss
                {name:"hitOrMissBG", url:"assets/images/hitOrMissBG.png"}
            ],
            textureMaps:[
                {
                    imageName:"symLogoAnim",
                    spriteSheet: {
                        frames: 26,
                        width: 130,
                        height: 118
                    }
                },
                {
                    imageName:"symWildAnim",
                    spriteSheet: {
                        frames: 41,
                        width: 130,
                        height: 118
                    }
                },
            ]    
        },
        sounds:{
            files:[
                {name:"anticipationSound",url:"assets/sounds/anticipation.m4a"},
                {name:"anticipationSoundEnd",url:"assets/sounds/anticipationEnd.m4a"},
                {name:"bgSound", url:"assets/sounds/baseGameBg.m4a"},
                {name:"freespinBG",url:"assets/sounds/freespinsBg.m4a"},
                {name:"winLine0",url:"assets/sounds/winline0.m4a"},
                {name:"winLine1",url:"assets/sounds/winline1.m4a"},
                {name:"winLine2",url:"assets/sounds/winline2.m4a"},
                {name:"winLine3",url:"assets/sounds/winline3.m4a"},
                {name:"winLine4",url:"assets/sounds/winline4.m4a"},
                {name:"winLine5",url:"assets/sounds/winline5.m4a"},
                {name:"winLine6",url:"assets/sounds/winline6.m4a"},  
                {name:"winLine7",url:"assets/sounds/winline7.m4a"},
                {name:"winLine8",url:"assets/sounds/winline8.m4a"},
                {name:"winLine9",url:"assets/sounds/winline9.m4a"},
            ],
            singleChannelFiles:[
                {name:"baseGameBG", url:"assets/sounds/baseGameBg.m4a"}
            ]
        },
        fonts:{
            files:[
                {name:"MyriadProRegular", url:"assets/fonts/MyriadProRegular.ttf"}
            ],
            styles:{
                families: ['MyriadProRegular', 'Arial'],
                fontFamily:"MyriadProRegular",
                fonSize:24,
                fill:"#ffffff"
            }
        }
    },
    background:{
        type:"Graphic",
        x:0,
        y:0,
        colour:"0x000000",
        rect:{
            x:0,
            y:0,
            width:1024,
            height:600
        },
        fullScreen:true
    },
    screens:{
        startScreen:"welcomeScreen",
        welcomeTransitionFade:true,
        children:[
            {
                id:"welcomeScreen",
                type:"Screen",
                children:[
                    {
                        type:"Graphic",
                        id:"welcomeBG",
                        x:0,
                        y:0,
                        colour:"0x000000",
                        rect:{
                            x:0,
                            y:0,
                            width:1024,
                            height:576
                        }
                    },
                    {
                        type:"Sprite",
                        imageName:"preLoaderLogo",
                        x:512,
                        y:288,
                        anchor:0.5
                    },
                    {
                        id:"startGameBtn",
                        type:"Button",
                        action:"Button",
                        sound:"btnClickSound",
                        anchor:0.5,
                        scale:0.8,
                        x:512,
                        y:490,
                        state:{
                            up:{
                                imageName:"startGameBtnUp"
                            },
                            disabled:{
                                imageName:"startGameBtnDisabled"
                            },
                            down:{
                                imageName:"startGameBtnDown"
                            }
                        }
                    }
                ]
            },
            {
                id:"reelScreen",
                type:"BrightLightCityReelScreen",
                children:[
                    {
                        id:"slotBG",
                        type:"Sprite",
                        imageName:"gameBG",
                        x:0,
                        y:0,
                    },
                    {
                        type:"Sprite",
                        imageName:"reelsFrame",
                        anchor:0.5,
                        x:512,
                        y:260
                    },
                    {
                        id:"reelContainer",
                        type:"BrightLightCityReelContainer",
                        x:174,
                        y:72,
                        children:[
                            {
                                type:"Container",
                                id:"reelsContainer",
                                x:0,
                                y:10,
                                reels:{
                                    //reelType:'AnimatedReel',
                                    reelType:'AnimatedSpineReel',
                                    anchor:0,
                                    reelStartPos:[0,0,0,0,0],
                                    reelWidth: 135,
                                    numberOfReels:5,
                                    reelMask: true,
                                    symbolWidth: 130,
                                    symbolHeight: 118,
                                    symbolsInView:3,
                                    spinSpeed:80,
                                    symbolXSpacer:0,
                                    symbolYSpacer:0,
                                    anticipateSpeed:150,
                                    reelBlur:3,
                                    reelDelay:100,
                                    reelStopDelay:300,
                                    reelSpinHighlightSymbols:['13','14','15','16'],
                                    rampUpSpeed:4000,
                                    rampUpDuration:400,
                                    rampUpDistance:40,
                                    bounceDistance:10,
                                    bounceDuration:80,
                                    bounceUpSpeed:5,
                                    symbolWinAlpha:0.5,
                                    reelstopSound:"reelstopSound",
                                    symbolAnimationSpeed:0.5,
                                    slotTextures:[
                                        "symTen", //0
                                        "symJack",//1
                                        "symQueen",//2
                                        "symKing",//3
                                        "symAce",//4
                                        "symTattoo",//5
                                        "symNightWorker",//6
                                        "symElvis",//7
                                        "symLogo",//8
                                        "symWild",//9
                                        "symBonusRed",//10
                                        "symBeerGoggles",//11
                                        "symBonusBlue",//12

                                    ],
                                    animations:{
                                        anchor:0.5,
                                        symbolAnimationSpeed:0.5,
                                        textures:[
                                            "symTen", //0
                                            "symJack",//1
                                            "symQueen",//2
                                            "symKing",//3
                                            "symAce",//4
                                            "symTattoo",//5
                                            "symNightWorker",//6
                                            "symElvis",//7
                                            "symLogoAnim",//8
                                            "symWildAnim",//9
                                            "symBonusRed",//10
                                            "symBeerGoggles",//11
                                            "symBonusBlue",//12
                                        ],/*
                                        spineTextures:[
                                            "",//0
                                            "",//1
                                            "",//2
                                            "",//3
                                            "",//4
                                            "",//5
                                            "",//6
                                            "",//7
                                            "",//8
                                            "",//9
                                            "",//10
                                            "",//11
                                            "",//12
                                        ],*/
                                        offsets:[
                                            {x:0,y:0,scale:1, enlarge:1.05},
                                            {x:0,y:0,scale:1, enlarge:1.05},
                                            {x:0,y:0,scale:1, enlarge:1.05},
                                            {x:0,y:0,scale:1, enlarge:1.05},
                                            {x:0,y:0,scale:1, enlarge:1.05},
                                            {x:0,y:0,scale:1, enlarge:1.05},
                                            {x:0,y:0,scale:1, enlarge:1.05},
                                            {x:0,y:0,scale:1, enlarge:1.05},
                                            {x:0,y:0,scale:1, enlarge:1.05},
                                            {x:0,y:0,scale:1, enlarge:1.05},
                                            {x:0,y:0,scale:1, enlarge:1.05},
                                            {x:0,y:0,scale:1, enlarge:1.05},
                                            {x:0,y:0,scale:1, enlarge:1},
                                            {x:0,y:0,scale:1, enlarge:1.05},
                                        ]
                                    }
                                },
                                /*mask:{
                                    id:"reelMask",
                                    tgt:"reelsContainer",
                                    type:"Graphic",
                                    anchor:0.5,
                                    x:0,
                                    y:-50,
                                    colour:"0xff00000",
                                    rect:{
                                        x:-82,
                                        y:-0,
                                        width:900,
                                        height:400
                                    }
                                }*/
                            },
                            {
                                type:"Sprite",
                                id:"anticipator",
                                imageName:"anticipator",
                                x:470,
                                y:-65,
                                visible: false
                            },
                        ]
                    },
                    {
                        type:"BrightLightCityAnimatedWinlineContainer",
                        id:"winlineContainer",
                        x:174,
                        y:72,
                        width:668,
                        height:353,
                        anchor: 0.5,
                        symbolSize:{width:140, height:101},
                        winlinePatterns:"",
                        winlineSize:6,
                        winlines:[
                            "0xff0000",
                            "0x0070bb",
                            "0x002585",
                            "0xffcfb9",
                            "0xff62a8",
                            "0xff8b00",
                            "0xff137c",
                            "0x00ff00",
                            "0xfbd920",
                            "0xff2b00",
                            "0x60ffff",
                            "0xffff00",
                            "0x29abe1",
                            "0xacff3e",
                            "0x29abe1",
                            "0xacacac",
                            "0x00a060",
                            "0xdb0000",
                            "0x5258ff",
                            "0x29abe1"
                        ]
                    },
                    {
                        id:"bonusMapStart",
                        action:"Button",
                        sound:"btnClickSound",
                        type:"Button",
                        anchor:0.5,
                        x:512,
                        y:288,
                        visible: false,
                        state:{
                            up:{
                                imageName:"bonusMapStart"
                            },
                            disabled:{
                                imageName:"bonusMapStart"
                            },
                            down:{
                                imageName:"bonusMapStart"
                            }
                        }
                    },
                    {
                        type:"Graphic",
                        id:"topBanner",
                        x:0,
                        y:0,
                        colour:"0x000000",
                        alpha:0.7,
                        rect:{
                            x:0,
                            y:0,
                            width:1024,
                            height:20
                        }
                    },
                    {
                        type:"Text",
                        text:"BRIGHT LIGHT CITY",
                        id:"topBannerText",
                        align:"left",
                        x:10,
                        y:1,
                        style:{
                            fontFamily:"MyriadProRegular",
                            colour:"#F7BA0C",
                            fontWeight:700,
                            fontSize:16,
                        }
                    },
                    {
                        type:"Container",
                        id:"maxWinsContainer",
                        x:512,
                        y:130,
                        visible: false,
                        children:[
                            {
                                type:"Text",
                                align:"right",
                                id:"maxWinText",
                                x:0,
                                y:0,
                                text:"MAXIMUM WIN REACHED",
                                style:{
                                    fillGradientType: "vertical",
                                    colour:["#fde000","#c76c00"],
                                    fontSize:78,
                                    dropShadow:"#000000",
                                    stroke: '#000000',
                                    dropShadow: true,
                                    strokeThickness: 5,
                                    dropShadowDistance:6,
                                    dropShadowBlur: 4,
                                    dropShadowAngle: Math.PI / 6,
                                    fontFamily:"germania_oneregular",
                                    fontWeight:700,
                                }
                            },
                        ]
                    },
                    {
                        id: "brightLightCityHitOrMiss",
                        type: "BrightLightCityHitOrMiss",
                        x:0,
                        y:0,
                        children:[
                            {
                                type:"Sprite",
                                imageName:"hitOrMissBG",
                                x:470,
                                y:-65,
                                visible: false
                            },
                        ]
                    }
                ]
            },
        ]
    },
    ui:{
        buttonPanelPos:"right",
        uiType:"default",
        showWinPanel:true,
        helpPages:[
    /*        {
                type:'page',
                children:[
                    {type:"Text",text:"HELP_P1_HEADING",x:20,y:0,align:'left',style:{colour:"#ffffff",fontSize:20,fontWeight:700,fontFamily:"MyriadProRegular"}},
                    {type:"Sprite",imageName:"Logo",scale:0.6,x:20,y:35},
                    {type:"Text",text:"HELP_P1_LINE1",x:120,y:30, align:'left',style:{colour:"#ffffff",fontSize:24,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:600}},
                    {type:"Sprite",imageName:"KingCharacter",scale:0.6,x:240,y:35},
                    {type:"Text",text:"HELP_P1_LINE2",x:340,y:30, align:'left',style:{colour:"#ffffff",fontSize:24,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:600}},
                    {type:"Sprite",imageName:"QueenCharacter",scale:0.6,x:460,y:35},
                    {type:"Text",text:"HELP_P1_LINE3",x:560,y:30, align:'left',style:{colour:"#ffffff",fontSize:24,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:600}},
                    {type:"Sprite",imageName:"PrincessCharacter",scale:0.6,x:20,y:145},
                    {type:"Text",text:"HELP_P1_LINE4",x:120,y:145, align:'left',style:{colour:"#ffffff",fontSize:24,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:600}},
                    {type:"Sprite",imageName:"reelSymbolA",scale:0.6,x:240,y:145},
                    {type:"Text",text:"HELP_P1_LINE5",x:340,y:150, align:'left',style:{colour:"#ffffff",fontSize:24,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:600}},
                    {type:"Sprite",imageName:"reelSymbolK",scale:0.6,x:460,y:145},
                    {type:"Text",text:"HELP_P1_LINE6",x:560,y:150, align:'left',style:{colour:"#ffffff",fontSize:24,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:600}},
                    {type:"Sprite",imageName:"reelSymbolQ",scale:0.6,x:20,y:255},
                    {type:"Text",text:"HELP_P1_LINE7",x:120,y:260, align:'left',style:{colour:"#ffffff",fontSize:24,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:600}},
                    {type:"Sprite",imageName:"reelSymbolJ",scale:0.6,x:240,y:255},
                    {type:"Text",text:"HELP_P1_LINE8",x:340,y:260, align:'left',style:{colour:"#ffffff",fontSize:24,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:600}},
                    {type:"Sprite",imageName:"reelSymbol10",scale:0.6,x:460,y:255},
                    {type:"Text",text:"HELP_P1_LINE9",x:560,y:260, align:'left',style:{colour:"#ffffff",fontSize:24,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:600}},
                ]
            },*/
            {
                type:'page',
                children:[
                    {type:"Text",text:"HELP_P1_HEADING",x:20,y:0,align:'left',style:{colour:"#ffffff",fontSize:20,fontWeight:700,fontFamily:"MyriadProRegular"}},
                    {type:"Text",text:["HELP_P2_LINE1","HELP_P2_LINE2","HELP_P2_LINE3","HELP_P2_LINE3a","HELP_P2_LINE3b"],x:20,y:40, align:'left',style:{colour:"#ffffff",fontSize:20,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:600}},
                    
                    {type:"Text",text:"HELP_P2_WILD_HEADING",x:20,y:140,align:'left',style:{colour:"#ffffff",fontSize:20,fontWeight:700,fontFamily:"MyriadProRegular"}},
                    
                    //{type:"Sprite",imageName:"WildFlag",scale:0.8,x:20,y:180},
                    {type:"Text",text:"HELP_P1_LINE10",x:160,y:200, align:'left',style:{colour:"#ffffff",fontSize:20,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:100}},
                    {type:"Text",text:"HELP_P2_WILD", x:260,y:170, align:'left',style:{colour:"#ffffff",fontSize:20,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:300}},
                    //{type:"Sprite",imageName:"Jester",scale:0.6,x:250,y:215},
                    //{type:"Sprite",imageName:"Shield",scale:0.6,x:350,y:215},
                    //{type:"Sprite",imageName:"Jackpot",scale:0.6,x:450,y:215},
                ]
            },
            {
                type:'page',
                children:[
                    {type:"Text",text:"HELP_P2_HEADING",x:20,y:0,align:'left',style:{colour:"#ffffff",fontSize:20,fontWeight:700,fontFamily:"MyriadProRegular"}},
                    //{type:"Sprite",imageName:"helpWinLines",x:20,y:40},
                ]
            },
            {
                type:'page',
                children:[
                    {type:"Text",text:"HELP_P3_HEADING",x:20,y:0,align:'left',style:{colour:"#ffffff",fontSize:20,fontWeight:700,fontFamily:"MyriadProRegular"}},
                    {type:"Text",text:["HELP_P3_LINE1","HELP_P3_LINE2"],x:20,y:40, align:'left',style:{colour:"#ffffff",fontSize:20,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:300}},
                 // {type:"Sprite",imageName:"fsBonusHelp1",scale:0.9,x:350,y:40},
                  // type:"Sprite",imageName:"fsBonusHelp2",scale:0.9,x:350,y:200},
                ]
            },
            {
                type:'page',
                children:[
                    {type:"Text",text:"HELP_P4_HEADING",x:20,y:0,align:'left',style:{colour:"#ffffff",fontSize:20,fontWeight:700,fontFamily:"MyriadProRegular"}},
                    {type:"Text",text:["HELP_P4_LINE1","HELP_P4_LINE2"],x:20,y:40, align:'left',style:{colour:"#ffffff",fontSize:20,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:300}},
                    //{type:"Sprite",imageName:"jpBonusHelp1",scale:0.9,x:350,y:40},
                    //{type:"Sprite",imageName:"jpBonusHelp2",scale:0.9,x:350,y:200},
                ]
            },
            {
                type:'page',
                children:[
                    {type:"Text",text:"HELP_P5_HEADING",x:20,y:0,align:'left',style:{colour:"#ffffff",fontSize:20,fontWeight:700,fontFamily:"MyriadProRegular"}},
                    {type:"Text",text:["HELP_P5_LINE1","HELP_P5_LINE2"],x:20,y:40, align:'left',style:{colour:"#ffffff",fontSize:19,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:300}},
                    //{type:"Sprite",imageName:"miniSlotBonusHelp1",scale:0.9,x:350,y:40},
                    //{type:"Sprite",imageName:"miniSlotBonusHelp2",scale:0.9,x:350,y:200},
                ]
            },
            {
                type:'page',
                children:[
                    {type:"Text",text:"HELP_P6_HEADING",x:20,y:0,align:'left',style:{colour:"#ffffff",fontSize:20,fontWeight:700,fontFamily:"MyriadProRegular"}},
                    {type:"Text",text:"HELP_P6_LINE1",x:20,y:40, align:'left',style:{colour:"#ffffff",fontSize:20,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:300}},
                    //{type:"Sprite",imageName:"trailHelp",scale:1,x:340,y:40},
                ]
            },
            {
                type:'page',
                children:[
                    {type:"Text",text:"HELP_P10_HEADING",x:20,y:0,align:'left',style:{colour:"#ffffff",fontSize:20,fontWeight:700,fontFamily:"MyriadProRegular"}},
                    {type:"Text",text:["HELP_P10_LINE1","HELP_P10_LINE2","HELP_P10_LINE3","HELP_P10_LINE4","HELP_P10_LINE5","HELP_P10_LINE6"],x:20,y:35, align:'left',style:{colour:"#ffffff",fontSize:20,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:600}},
                ]
            },
            {
                type:'page',
                children:[
                    {type:"Text",text:"HELP_P11_HEADING",x:20,y:0,align:'left',style:{colour:"#ffffff",fontSize:20,fontWeight:700,fontFamily:"MyriadProRegular"}},
                    {type:"Text",text:["HELP_P11_LINE1","HELP_P11_LINE2","HELP_P11_LINE3","HELP_P11_LINE4","HELP_P11_LINE5","HELP_P11_LINE6","HELP_P11_LINE7"],x:20,y:35, align:'left',style:{colour:"#ffffff",fontSize:20,fontFamily:"MyriadProRegular",multiline:true, multilineWidth:600}},
                ]
            }
        ]  
    },
    locale:{
        MS_BONUS_TITLE:'Shield Mini Slot Bonus',
        MS_BONUS_DESCRIPTION: 'Start spinning the shields by pressing the button. When the shields stop they reveal either a horse, lion, or drake symbol. You are awarded the bet amount multiplied by the multiplier indicated by the legend.',

        JP_JACKPOT_TITLE:'Royal Crest Bonus',
        JP_JACKPOT_DESCRIPTION: 'Click the spin button to start the crest symbols moving. If the crest finishes with 3 matching symbols (horse,lion,dragon), then you will win the associated Jackpot.  A consolation prize can be won',

        FS_BONUS_TITLE:'Freespins Bonus',
        FS_BONUS_DESCRIPTION: 'During the feature two wild symbols will be locked into place on the middle row on reel 2 and 4.',
        FS_BONUS_DESCRIPTION2: 'Extra',

        HELP_P1_HEADING:'PAYTABLE',
        HELP_P1_LINE1:'5 x500 \n4 x200 \n3 x20 \n2 x5',
        HELP_P1_LINE2:'5 x250 \n4 x150 \n3 x20 \n2 x2',
        HELP_P1_LINE3:'5 x200 \n4 x100 \n3 x15 \n2 x2',
        HELP_P1_LINE4:'5 x120 \n4 x40 \n3 x10 \n2 x2',
        HELP_P1_LINE5:'5 x80 \n4 x20 \n3 x5',
        HELP_P1_LINE6:'5 x80 \n4 x20 \n3 x5',
        HELP_P1_LINE7:'5 x60 \n4 x10 \n3 x2',
        HELP_P1_LINE8:'5 x60 \n4 x10 \n3 x2',
        HELP_P1_LINE9:'5 x60 \n4 x10 \n3 x2',

        HELP_P1_LINE10:'5 x500 \n4 x200 \n3 x20 \n2 x5',

        HELP_P2_HEADING:'WINLINES',
        HELP_P2_LINE1:'All line wins multiplied by bet per line on winning line. ',
        HELP_P2_LINE2:'Line wins will pay from left to right only. ',
        HELP_P2_LINE3:'Only the highest win per bet line is paid. ',
        HELP_P2_LINE3a:'All individual winning lines will be displayed, added to the total win and paid to the player. ',
        HELP_P2_LINE3b:'It is possible to win multiple bonuses on a single spin. ',

        HELP_P2_WILD_HEADING:'Wild Symbols',
        HELP_P2_WILD:'Wild Symbols substitutes for any Icon, except:',

        HELP_P3_HEADING:'Jester Bonus (Scatter Symbol)',
        HELP_P3_LINE1:'If 3 or more Jester symbols land then the Free spins feature is triggered. 3 symbols give 10 free spins, 4 symbols give 20 free spins and 5 symbols give 30 free spins.\n',
        HELP_P3_LINE2:'During the feature two wild symbols will be locked into place on the middle row on reel 2 and 4.',

        HELP_P4_HEADING:'Royal Crest Bonus (Scatter Symbol)',
        HELP_P4_LINE1:'The Crest bonus game is triggered by 3 crest symbols landing on the reels. The bonus game starts by the drawbridge closing, revealing the royal crest.\n',
        HELP_P4_LINE2:'The players clicks the spin button to start the crest symbols moving. If the crest finishes with 3 matching symbols (horse,lion,dragon), then the player will win the associated Jackpot.  A consolation prize can be won',

        HELP_P5_HEADING:'Shield Mini Slot Bonus (Scatter Symbol)',
        HELP_P5_LINE1:'When 3 shield symbols are in view (appears on only reels 2 3 and 4), this triggers the Mini Slot Bonus. The player is awarded 3, 5 or 10 spins. The mini slot has 3 shields. \n',
        HELP_P5_LINE2:'The shields start spinning by pressing spin button. When the shields stop spinning they reveal either a horse, lion, or drake symbol. The player is awarded the bet amount multiplied by the multiplier indicated by the legend.',

        HELP_P6_HEADING:'Portcullis Bonuses',
        HELP_P6_LINE1:'The Portcullis bonus will be triggered randomly. The left Portcullis gate rises with a cash amount shown.  The right Portcullis gate then opens with multipliers coming from the flames, when the game stops on the multiplier the player is awarded the amount in the left tower multiplied by the amount in the right tower \n',

        HELP_P10_HEADING:'PROGRESSIVE CASHPOTS',
        HELP_P10_LINE1:'Progressive Cashpots: There are three progressive cashpots which are labelled Horse(Green), Lion(Blue) and Drake(Red). During each spin a proportion of the money staked is added to each cashpot. Each pot will also have a base re-seed value.',
        HELP_P10_LINE2:'Horse 500 / Lion 2,000 / Drake 10,000. ',
        HELP_P10_LINE3:'Each winning progressive will be delivered via the Map Bonus Game scatter bonus. ',
        HELP_P10_LINE4:'The progressive cashpots are available in "Real Play" only.',
        HELP_P10_LINE5:'The seed value is topped up by contributions of 1.8% from stakes on the game split bronze 0.9% / silver 0.54% / gold 0.36% across the pots and there is no ceiling value. \n',
        HELP_P10_LINE6:'Total return to player from the progressive jackpot is 2%. This is split 1.8% contribution to the jackpot and 0.2% to reseeding once the jackpot has been won. This makes up part of the total game return to player which is given separately. \n',

        HELP_P11_HEADING:'INFORMATION',
        HELP_P11_LINE1:'Malfunction voids all pays and plays. ',
        HELP_P11_LINE2:'This game is random.\n',
        HELP_P11_LINE3:'All wins pay from left to right, right to left and center on each winline. Only the highest win on each winline will be awarded.\n\n',
        HELP_P11_LINE4:'The outcome of any game or feature is not necessarily that shown by the odds displayed.\n\n',
        HELP_P11_LINE5:'This game has a theoretical Return to Player (RTP) of 94.86%.\n',
        HELP_P11_LINE6:'The maximum payout in one spin in this game is {MAX_WIN}.\n\n',
        HELP_P11_LINE7:'Due to high production values of this game, lower-tier hardware may run into compatibility issues and in certain instances, you will not be able to play.'
    }

}