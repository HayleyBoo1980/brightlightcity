HoneyPot.BLCOpenBetChainConnection  = function(config, callback){
    HoneyPot.OpenBetReelChainConnection.call(this, config, callback)
}

HoneyPot.BLCOpenBetChainConnection.prototype = Object.create(HoneyPot.OpenBetReelChainConnection.prototype);
HoneyPot.BLCOpenBetChainConnection.prototype.constructor = HoneyPot.BLCOpenBetChainConnection;

HoneyPot.OpenBetReelChainConnection.prototype.getChainHeaderDetails = function(){
    var freePlayStr = "No";
    var isGuest = "No";

    var gameString = '<GameDetails class="' + this.gameState.chainPlay.gameClass + '" name="' + this.gameState.chainPlay.gameName + '" free_play="'+freePlayStr+'" channel="' + this.channel + '" />';
    var customerString = '<Customer cookie= "'+ this.cookie+ '" balance="0.00" is_guest="'+isGuest+'" affiliate="" />';


    return gameString + customerString;
}

HoneyPot.BLCOpenBetChainConnection.prototype.sendInitChainGame = function(cb)
{

    var xml = this.xmlMeta + '<GameRequest><Header>' + this.getChainHeaderDetails()+ '</Header> <Init payout="Yes" definition="Yes" accumulators="Yes" /></GameRequest>';
    this.sendToServer(xml, this.initChainGameResponse.bind(this), this.connectionError.bind(this));

};

HoneyPot.BLCOpenBetChainConnection.prototype.initChainGameResponse = function(cb) {

    console.log("initChainGameResponse");
    this.sendPlayChain();

}

HoneyPot.BLCOpenBetChainConnection.prototype.sendPlayChain = function(cb)
{

    var xml = this.xmlMeta + '<GameRequest><Header>' + this.getChainHeaderDetails()+ '</Header><Play sequence_no="1" stake="2.00"><MBonusWheel action="Bet"></MBonusWheel></Play></GameRequest>';
    this.sendToServer(xml, this.playChainResponse.bind(this), this.connectionError.bind(this));

};

HoneyPot.BLCOpenBetChainConnection.prototype.playChainResponse = function(data)
{
    console.log("playChainResponse");
}

HoneyPot.BLCOpenBetChainConnection.prototype.sendChainBet = function(cb)
{

            var xml = this.xmlMeta + '<GameRequest><Header>' + this.getChainHeaderDetails()+ '</Header><Play sequence_no="1" stake="'+this.gameState.stake +'"><MBonusWheel action="Bet"></MBonusWheel></Play></GameRequest>';
            this.sendToServer(xml, this.chainBetResponse.bind(this), this.connectionError.bind(this));

};

HoneyPot.BLCOpenBetChainConnection.prototype.chainBetResponse = function(cb) {
    console.log("chain bet response - received");
}



HoneyPot.BLCOpenBetChainConnection.prototype.sendCollect = function(cb)
{

            var xml = this.xmlMeta + '<GameRequest><Header>' + this.getChainHeaderDetails()+ '</Header><Play sequence_no="1" stake="'+this.stake+'"><MBonusWheel action="Collect"></MBonusWheel></Play></GameRequest>';

            this.sendToServer(xml, this.collectBetResponse.bind(this), this.connectionError.bind(this));


};

HoneyPot.BLCOpenBetChainConnection.prototype.collectBetResponse = function(cb) {
    console.log("chain bet response - received");
}



HoneyPot.BLCOpenBetChainConnection.prototype.sendCloseRequest = function(cb)
{
var chainstring = "<MasterState state='C' stakes='20.00' winnings='0.00' next_stake='20.00' cash_pot='0.00' start_game_state='F' chain_stake='20.00' current_winnings='0.00' > <ActiveStack> </ActiveStack> <OpenStack> <ChainState cg_id='6301' class='MBWheel' name='BrightLightCityHitItOrQuitItBonus' stack_order='0' start_stake='20.00' id='' interaction='' game_state='F' stake_type='F'></ChainState></OpenStack> </MasterState>"
    console.log("Senclose request from chain")
    this.closeResponseCallBack = cb;
    if(this.serverError){
        this.closeResponseCallBack();
    }else{
        if(this.gameState.chainPlay != null){
            var xml = this.xmlMeta + '<GameRequest><Header>' + this.getChainHeaderDetails()+ '</Header><Close/>'+chainstring+ '</GameRequest>';
            this.sendToServer(xml, this.closeChainResponse.bind(this), this.connectionError.bind(this));
        }else{
            var xml = this.xmlMeta + '<GameRequest><Header>' + this.getHeaderDetails()+ '</Header><Close/></GameRequest>';
            this.sendToServer(xml, this.closeResponse.bind(this), this.connectionError.bind(this));
        }
    }
};






