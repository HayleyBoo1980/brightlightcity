/**
npm Dev dependencies

 "devDependencies": {
    "gulp": "^4.0.2",
    "gulp-concat": "^2.6.1",
    "gulp-order": "^1.2.0",
    "gulp-rename": "^1.4.0",
    "gulp-sourcemaps": "^2.6.5",
    "gulp-uglify": "^3.0.2",
     "gulp-javascript-obfuscator": "^1.1.6"
  }

**/

/**
 * Created by Ant on 10/03/2020.
 */


var fs = require('fs');
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var order = require('gulp-order');
var javascriptObfuscator = require('gulp-javascript-obfuscator');
var args   = require('yargs').argv;

var gameName = args.gameName;

var paths = {
    scriptsOutput: 'dist/CashInTheKeep',
    scripts: [
      'app/config.js',
      'app/display/cashInTheKeepReelScreen.js',  
      'app/display/cashInTheKeepReelContainer.js',
      'app/display/cashInTheKeepAnimatedSpineReel.js', 
      'app/display/towerContainer.js', 
      'app/display/cashInTheKeepJackpotWinPanel.js',
      'app/connection/CIKOpenBetReelConnection.js',
      'app/connection/CIKGameState.js',
      'app/display/shieldBonusContainer.js',
      'app/display/freeSpinsBonusContainer.js.js',
      'app/display/trailBonusContainer.js',
      'app/display/jackPotBonusContainer.js',
      'app/cashInTheKeep.js'

    ],
    scriptOrder: [
      'app/config.js',
      'app/display/cashInTheKeepReelScreen.js',  
      'app/display/cashInTheKeepReelContainer.js',
      'app/display/cashInTheKeepAnimatedSpineReel.js', 
      'app/display/towerContainer.js', 
      'app/display/cashInTheKeepJackpotWinPanel.js',
      'app/connection/CIKOpenBetReelConnection.js',
      'app/connection/CIKGameState.js',
      'app/display/shieldBonusContainer.js',
      'app/display/freeSpinsBonusContainer.js.js',
      'app/display/trailBonusContainer.js',
      'app/display/jackPotBonusContainer.js',
      'app/cashInTheKeep.js'
    ]
};

function handleError(err) {
    console.log(err.toString());
    this.emit('end');
};


gulp.task('createIndex', async function(cb){
  fs.writeFile('index.html', '<!doctype html>\r\n<html>\r\n<head>\r\n  <meta name="viewport" id ="CGEViewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />\r\n    <meta name="apple-mobile-web-app-capable" content="yes" />\r\n  <meta charset="utf-8">\r\n  <title>'+gameName+'</title>\r\n  <script src="honeypot/vendor.js"></script>\r\n  <script src="honeypot/app.js"></script>\r\n  <script src="app/config.js"></script>\r\n  <script src="app/'+gameName+'Game.js"></script>\r\n  <script src="app/display/'+gameName+'ReelScreen.js"></script>\r\n  <script src="app/connection/'+gameName+'Connection.js"></script>\r\n  <link rel="stylesheet" href="assets/game.css">\r\n</head>\r\n<body>\r\n  <div id="gameLoader">loading<b>.</b></div>\r\n  <script type="text/javascript">\r\n    var game = new HoneyPot.'+gameName+'Game();\r\n    game.init();\r\n    $(window).resize(function(){\r\n      game.resize();\r\n    });\r\n  </script>\r\n</body>\r\n</html>',  function(err) {
    console.log('index.html created');
  });
});

gulp.task('createGame', async function(cb){
  fs.writeFile('app/'+gameName+'Game.js', 'HoneyPot.'+gameName+'Game = function(){\r\n  HoneyPot.Game.call(this);\r\n}\r\nHoneyPot.'+gameName+'Game.prototype = Object.create(HoneyPot.Game.prototype);\r\nHoneyPot.'+gameName+'Game.prototype.constructor = HoneyPot.'+gameName+'Game;\r\nHoneyPot.'+gameName+'Game.prototype.welcomeTransitionComplete = function(){\r\n  HoneyPot.Game.prototype.welcomeTransitionComplete.call(this);\r\n  this.app.playSound("bgSound",true,0.6);\r\n}\r\nHoneyPot.'+gameName+'Game.prototype.startGIP = function(){\r\n  this.canvas.ticker.maxFPS = this.gameMaxFPS;\r\n  if(this.gameState.slotState && this.gameState.slotState.action == "freeSpinReels"){\r\n    this.topBar.setwin(this.gameState.slotState.totalWinnings - this.gameState.slotState.currentWinnings);\r\n    this.gameUI.setWinText(this.gameState.slotState.totalWinnings - this.gameState.slotState.currentWinnings);\r\n    this.screens["reelScreen"].currentWinnings = this.gameState.slotState.totalWinnings - this.gameState.slotState.currentWinnings;\r\n    this.gameUI.hideButtonUI();\r\n    this.gameState.inFreeSpins = true;\r\n    this.screens["reelScreen"].reelContainer.inFreespins = true;\r\n  }else{\r\n    this.gameUI.fadeOutButtons();\r\n  }\r\n  this.topBar.gameStarted(true);\r\n  this.screens["reelScreen"].spin();\r\n  this.playRequestReceived();\r\n}',  function(err) {
    console.log(gameName+'Game.js created');
  });
});

gulp.task('createReelScreen', async function(cb){
  fs.writeFile('app/display/'+gameName+'ReelScreen.js', 'HoneyPot.'+gameName+'ReelScreen = function(data){\r\n  HoneyPot.ReelScreen.call(this,data);\r\n}\r\nHoneyPot.'+gameName+'ReelScreen.prototype = Object.create(HoneyPot.ReelScreen.prototype);\r\nHoneyPot.'+gameName+'ReelScreen.prototype.constructor = HoneyPot.'+gameName+'ReelScreen;\r\nHoneyPot.'+gameName+'ReelScreen.prototype.initComponents = function(){\r\n  HoneyPot.ReelScreen.prototype.initComponents.call(this);\r\n}\r\nHoneyPot.'+gameName+'ReelScreen.prototype.startFreeSpins = function(){\r\n  this.gameState.inFreeSpins = true;\r\n  this.closeSpin();  \r\n}\r\nHoneyPot.'+gameName+'ReelScreen.prototype.showScatterBonus = function(){\r\n  var bonusDef = this.gameState.getSlotDef().getSlotBonusByID(/*bpnusID*/);\r\n  this.reelContainer.resetReelSymbols();\r\n  switch(bonusDef.name){\r\n    case "Freespins":\r\n      this.startFreeSpins();\r\n      break;\r\n  }\r\n}',  function(err) {
    console.log(gameName+'ReelScreen.js created');
  });
});

gulp.task('createCss', async function(cb){
  fs.writeFile('assets/game.css', 'html{\r\n  height: 100%;\r\n  margin: 0;\r\n  padding: 0;\r\n}\r\nbody{\r\n  height: 100%;\r\n  min-height: 100%;\r\n  overflow: hidden;\r\n  background-color: #000;\r\n  position: relative;\r\n  margin: 0px;\r\n  padding: 0px;\r\n  color:#FFF;\r\n  font-family: "MyriadProRegular",Arial, Helvetica, sans-serif;\r\n  -ms-user-select: none;\r\n    -ms-touch-select: none;\r\n    -ms-touch-action: none;\r\n    -moz-user-select: none;\r\n    -webkit-user-select: none;\r\n    -webkit-touch-callout: none;\r\n    -webkit-tap-highlight-color: rgba(0,0,0,0); \r\n    -webkit-focus-ring-color: rgba(0, 0, 0, 0);\r\n    user-select: none;\r\n    -webkit-touch-callout: none;        \r\n    -webkit-focus-ring-color:  rgba(0, 0, 0, 0);        \r\n    -webkit-text-size-adjust: auto;    \r\n    -o-background-size: 100% 100%;\r\n    -ms-background-size: 100% 100%;\r\n    -moz-background-size: 100% 100%;\r\n    -webkit-background-size: 100% 100%;\r\n    background-size: 100% 100%;\r\n}\r\n*{\r\n  margin:0;\r\n  padding:0;\r\n}\r\n@font-face {\r\n  font-family: "MyriadProRegular";\r\n  font-style: normal;\r\n  font-weight: normal; \r\n  src: url("../honeypot/ui/fonts/MyriadPro-Regular.eot"); \r\n  src: url("../honeypot/ui/fonts/MyriadPro-Regular.eot?#iefix") format("embedded-opentype"), \r\n         url("../honeypot/ui/fonts/MyriadPro-Regular.woff") format("woff"),\r\n         url("../honeypot/ui/fonts/MyriadPro-Regular.ttf")  format("truetype")\r\n}\r\n@font-face {\r\n  font-family: "MyriadProRegular";\r\n  font-style: bold;\r\n  font-weight: 700; \r\n  src: url("../honeypot/ui/fonts/MyriadPro-Bold.eot"); \r\n  src: url("../honeypot/ui/fonts/MyriadPro-Bold.eot?#iefix") format("embedded-opentype"), \r\n         url("../honeypot/ui/fonts/MyriadPro-Bold.woff") format("woff"), \r\n         url("../honeypot/ui/fonts/MyriadPro-Bold.ttf")  format("truetype")\r\n}\r\n#gameLoader{\r\n  position: absolute;\r\n  left: 45%;\r\n  top: 50%;\r\n  -o-transform: translate(-50%, 0);\r\n  -o-transform: scale(2.0);\r\n  -ms-transform: translate(-50%, 0);\r\n  -ms-transform: scale(2.0);\r\n  -moz-transform: translate(-50%, 0);\r\n  -moz-transform: scale(2.0);\r\n  -webkit-transform: translate(-50%, 0);\r\n  -webkit-transform: scale(2.0);\r\n  transform: translate(-50%, 0);\r\n  transform: scale(2.0);\r\n}',  function(err) {
    console.log('game.css created');
  });
});

gulp.task('createConnection', async function(cb){
  fs.writeFile('app/connection/'+gameName+'Connection.js', 'HoneyPot.'+gameName+'GameConnection = function(config, callback){\r\n  HoneyPot.DummyReelConnection.call(this,config, callback);\r\n  this.initData = {\r\n    slotDef:{\r\n        symbols:[/*reels symbols*/],\r\n        numWinLines:5,\r\n        winlines:[[/*winline0*/],\r\n            [/*winline1*/],\r\n            [/*winline2*/],\r\n            [/*winline3*/],\r\n            [/*winline4*/]\r\n        ],\r\n        reelSet:[\r\n          {\r\n            index:"0",\r\n            name:"base",\r\n            numReels:5,\r\n            reels:[\r\n              [/*reel0*/],\r\n              [/*reel1*/],\r\n              [/*reel2*/],\r\n              [/*reel3*/],\r\n              [/*reel4*/]\r\n            ]\r\n          }\r\n        ],\r\n        bitMask: [\r\n          [0,0,0,0,0],\r\n          ["x",0,0,0,0],\r\n          ["x","x",0,0,0],\r\n          ["x","x","x",0,0],\r\n          ["x","x","x","x",0],\r\n          ["x","x","x","x","x"]\r\n        ]\r\n      }\r\n    };\r\n}\r\nHoneyPot.'+gameName+'GameConnection.prototype = Object.create(HoneyPot.DummyReelConnection.prototype);\r\nHoneyPot.'+gameName+'GameConnection.prototype.constructor = HoneyPot.'+gameName+'GameConnection;\r\nHoneyPot.'+gameName+'GameConnection.prototype.init = function(){\r\n    this.setupAccount();\r\n    this.connectToServer(this.initData);\r\n}',  function(err) {
    console.log(gameName+'GameConnection.js created');
  });
});

gulp.task('createConfig', async function(cb){
  fs.writeFile('app/config.js', 'var gameConfig = {\r\n  game:{\r\n    version:"1.0.0",\r\n    gameName:"'+gameName+' Game",\r\n    gameType:"slot",\r\n    channel:"I",\r\n    gameWidth:1024,\r\n    gameHeight:576,\r\n    antialias:true,\r\n    transparent:false,\r\n    forceCanvas:true,\r\n    resolution:1\r\n  },\r\n  connection:{\r\n    connectionURL: "",\r\n    freeBets: "Yes",\r\n      freePlay: true,\r\n        isGuest: "No",\r\n        cookie: "",\r\n        connectionType: "'+gameName+'GameConnection",\r\n        gameClass: "Slot",\r\n        gameName: "'+gameName+'",\r\n        freeGameName: "Free'+gameName+'",\r\n        channel: "I",\r\n        connectionTimeOut: 30000\r\n  },\r\n  assets:{\r\n    images:{\r\n      files:[\r\n        /*{name:"gamebg", url:"assets/images/gameBG.png"},\r\n        {name:"reelframe", url:"assets/images/reelframe.png"},\r\n        {name:"logo", url:"assets/images/logo.png"},\r\n        {name:"reelSymbols", url:"assets/images/symbols.png"},\r\n        {name:"loadingBG", url:"assets/images/loadingBG.jpg"}*/\r\n      ],\r\n      textureMaps:[\r\n        /*{\r\n          imageName:"reelSymbols",\r\n          imagePositions:[\r\n            {\r\n              id:"symbolO",\r\n              x:0,\r\n              y:0,\r\n              width:137,\r\n              height:137\r\n            },\r\n            {\r\n              id:"symbol1",\r\n              x:137,\r\n              y:0,\r\n              width:137,\r\n              height:137\r\n            },\r\n            {\r\n              id:"symbol2",\r\n              x:274,\r\n              y:0,\r\n              width:137,\r\n              height:137\r\n            }\r\n          ]\r\n        }*/\r\n      ]    \r\n    },\r\n    sounds:{\r\n      files:[\r\n        /*{name:"bgSound", url:"assets/sounds/baseGameBg.m4a"},\r\n        {name:"reelspinSound", url:"assets/sounds/reelSpin.m4a"},\r\n        {name:"reelstopSound", url:"assets/sounds/reelStop.m4a"},\r\n        {name:"lineWinSound", url:"assets/sounds/winLine.m4a"}*/\r\n      ],\r\n      singleChannelFiles:[\r\n        /*{name:"bgSound", url:"assets/sounds/baseGameBg.m4a"}*/\r\n      ]\r\n    },\r\n    fonts:{\r\n      files:[\r\n        {name:"MyriadProRegular", url:"assets/fonts/MyriadProRegular.ttf"}\r\n      ],\r\n      styles:{\r\n        families: ["MyriadProRegular", "Arial"],\r\n        fontFamily:"MyriadProRegular",\r\n        fonSize:24,\r\n        fill:"#ffffff"\r\n      }\r\n    }\r\n  },\r\n  background:{\r\n    type:"Graphic",\r\n    x:0,\r\n    y:0,\r\n    colour:"0x000000",\r\n    rect:{\r\n      x:0,\r\n      y:0,\r\n      width:1024,\r\n      height:600\r\n    },\r\n    fullScreen:true\r\n  },\r\n  ui:{\r\n    buttonPanelPos:"right",\r\n    uiType:"default",\r\n    showWinPanel:true,\r\n    helpPages:[\r\n\r\n    ]  \r\n  },\r\n  screens:{\r\n    startScreen:"welcomeScreen",\r\n    welcomeTransitionFade:true,\r\n    children:[\r\n      {\r\n        id:"welcomeScreen",\r\n        type:"Screen",\r\n        children:[\r\n          /*{\r\n            type:"Sprite",\r\n            imageName:"loadingBG",\r\n            x:0,\r\n            y:0\r\n          },*/\r\n          {\r\n            type:"Button",\r\n            action:"Button",\r\n            sound:"btnClickSound",\r\n            id:"startGameBtn",\r\n            scale:0.8,\r\n            anchor:0.5,\r\n            x:512,\r\n            y:460,\r\n            state:{\r\n              up:{\r\n                imageName:"startGameBtnUp"\r\n              },\r\n              disabled:{\r\n                imageName:"startGameBtnDisabled"\r\n              },\r\n              down:{\r\n                imageName:"startGameBtnDown"\r\n              }\r\n            }\r\n          }\r\n\r\n        ]\r\n      },\r\n      {\r\n        id:"reelScreen",\r\n        type:"ReelScreen",\r\n        children:[\r\n          /*{\r\n            type:"Sprite",\r\n            x:0,\r\n            y:0,\r\n            imageName:"gamebg"\r\n          },*/\r\n          /*{\r\n            type:"Sprite",\r\n            x:90,\r\n            y:36,\r\n            imageName:"reelframe"\r\n          },*/\r\n          {\r\n            type:"ReelContainer",\r\n            id:"reelContainer",\r\n            x:169,\r\n            y:50,\r\n            children:[\r\n              {\r\n                type:"Container",\r\n                id:"reelsContainer",\r\n                x:0,\r\n                y:0,\r\n                reels:{\r\n                  reelStartPos:[0,0,0,0,0],\r\n                  reelWidth: 137,\r\n                  numberOfReels:5,\r\n                  symbolWidth: 137,\r\n                  symbolHeight: 137,\r\n                  symbolsInView:3,\r\n                  spinSpeed:35,\r\n                  anchor:0,\r\n                  symbolXSpacer:0,\r\n                  symbolYSpacer:0,\r\n                  reelBlur:10,\r\n                  reelDelay:5,\r\n                  reelStopDelay:10,\r\n                  rampUpSpeed:5,\r\n                  rampUpDistance:15,\r\n                  bounceDistance:20,\r\n                  bounceUpSpeed:5,\r\n                  symbolWinAlpha:0.2,\r\n                  reelstopSound:"reelstopSound",\r\n                  symbolAnimationSpeed:0.5,\r\n                  slotTextures:[\r\n                    /*"symbolO",\r\n                    "symbol1",\r\n                    "symbol2"*/\r\n                  ]\r\n                },\r\n                mask:{\r\n                  id:"reelMask",\r\n                  tgt:"reelsContainer",\r\n                  type:"Graphic",\r\n                  x:0,\r\n                  y:0,\r\n                  colour:"0xff0000",\r\n                  rect:{\r\n                    x:0,\r\n                    y:0,\r\n                    width:685,\r\n                    height:411\r\n                  }\r\n                }\r\n              }\r\n            ]\r\n          },\r\n          /*{\r\n            type:"Sprite",\r\n            x:900,\r\n            y:440,\r\n            imageName:"logo"\r\n          },*/\r\n          {\r\n            type:"WinlineContainer",\r\n            id:"winlineContainer",\r\n            x:169,\r\n            y:50,\r\n            width:685,\r\n            height:411,\r\n            symbolSize:{width:137, height:137},\r\n            winlinePatterns:"",\r\n            winlineSize:6,\r\n            winlines:[\r\n              /*"0xff0000",\r\n              "0x0070bb",\r\n              "0x002585"*/\r\n            ]\r\n          },\r\n        ]\r\n      }  \r\n    ]\r\n  }\r\n}',  function(err) {
    console.log('config.js created');
  });
});

gulp.task('createDirectories', function () {
    return gulp.src('*.*', {read: false})
        .pipe(gulp.dest('./app'))
        .pipe(gulp.dest('./app/display'))
        .pipe(gulp.dest('./app/connection'))
        .pipe(gulp.dest('./assets'))
        .pipe(gulp.dest('./assets/fonts'))
        .pipe(gulp.dest('./assets/images'))
        .pipe(gulp.dest('./assets/images/@2x'))
        .pipe(gulp.dest('./assets/sounds'));
});

gulp.task('scripts', function () {
    return gulp.src(paths.scripts)
        .pipe(order(paths.scriptOrder, { base: './' }))
        .pipe(concat('game.js'))
        .on('error', handleError)
        .pipe(javascriptObfuscator())
        .on('error', handleError)
        .pipe(gulp.dest(paths.scriptsOutput))

});
// Concatenate & Minify JS
gulp.task('scriptswatch', function () {
    return gulp.src(paths.scripts)
        .pipe(order(paths.scriptOrder, { base: './' }))
        .pipe(sourcemaps.init())
        .pipe(concat('game.js'))
        .on('error', handleError)
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .on('error', handleError)
        .pipe(gulp.dest(paths.scriptsOutput))

});
gulp.task('copyassets', function () {
   return gulp.src('assets/**/*', { base: './' })
        .on('error', handleError)
        .pipe(gulp.dest(paths.scriptsOutput));
});
gulp.task('copyfiles', function () {
   return gulp.src(['index.html'])
        .on('error', handleError)
        .pipe(gulp.dest(paths.scriptsOutput));
});
gulp.task('copyEngine', function () {
   return gulp.src('honeypot/**/*',{ base: './' })
        .on('error', handleError)
        .pipe(gulp.dest(paths.scriptsOutput));
});

// Watch Files For Changes
gulp.task('watch', function () {
    gulp.watch(paths.scripts, gulp.series('scriptswatch')).on('error', handleError);
});

gulp.task('build', gulp.series('scripts','copyassets','copyfiles','copyEngine'));

// Default Task
gulp.task('default', gulp.series('watch'));

gulp.task('create', gulp.series('createDirectories','createIndex','createGame','createReelScreen','createConnection','createConfig','createCss'));

